<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Update_Page</title>
  </head>
  <body>
    

    <div class="container p-5">

      <h1>Update Record</h1>
      <hr>

      <!-- PHP read record by ID -->
      <?php 

      $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');

      include 'config/database.php';

      try {

        // prepare query
        $query = "SELECT id, studentName, fatherName, dob, gender, mobileNo, email, address, studyProgram, hobby, image FROM records WHERE id = ? LIMIT 0, 1";
        $statement = $conn->prepare($query);

        $statement->bindParam(1, $id);

        $statement->execute();

        // store return value 
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        // values goes in form
        $studentName = $row['studentName'];
        $fatherName = $row['fatherName'];
        $dob = $row['dob'];
        $gender = $row['gender'];
        $mobileNo = $row['mobileNo'];
        $email = $row['email'];
        $address = $row['address'];
        $studyProgram = $row['studyProgram'];
        $hobby = $row['hobby'];
        $image = htmlspecialchars($row['image'], ENT_QUOTES);

        
      } catch (PDOException $e) {

        die('ERROR:' . $e->getMessage());
        
      }

       ?>

       <!-- PHP post to update record -->
       <?php 

       if ($_POST) {

        try {

          // update query
          $query = "UPDATE records SET studentName=:studentName, fatherName=:fatherName, dob=:dob, gender=:gender, mobileNo=:mobileNo, email=:email, address=:address, studyProgram=:studyProgram, hobby=:hobby, image=:image WHERE id=:id";

          $statement = $conn->prepare($query);

          
          $studentName = htmlspecialchars(strip_tags($_POST['studentName']));
          $fatherName = htmlspecialchars(strip_tags($_POST['fatherName']));
          $dob = htmlspecialchars(strip_tags($_POST['dob']));
          $gender = htmlspecialchars(strip_tags($_POST['gender']));
          $mobileNo = htmlspecialchars(strip_tags($_POST['mobileNo']));
          $email = htmlspecialchars(strip_tags($_POST['email']));
          $address = htmlspecialchars(strip_tags($_POST['address']));
          $studyProgram = htmlspecialchars(strip_tags($_POST['studyProgram']));
          $hobby = htmlspecialchars(implode(",", $_POST['hobby']));
          $image =! empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";
          $image = htmlspecialchars(strip_tags($image));


          $statement->bindParam(':studentName', $studentName);
          $statement->bindParam(':fatherName', $fatherName);
          $statement->bindParam(':dob', $dob);
          $statement->bindParam(':gender', $gender);
          $statement->bindParam(':mobileNo', $mobileNo);
          $statement->bindParam(':email', $email);
          $statement->bindParam(':address', $address);
          $statement->bindParam(':studyProgram', $studyProgram);
          $statement->bindParam(':hobby', $hobby);
          $statement->bindParam(':image', $image);
          $statement->bindParam(':id', $id);


          if ($statement->execute()) {

            echo "<div class='alert alert-success'>

              Record is updated.
              
            </div>";

          // check image field is nto empty
          if ($image) {

            $target_directory = "uploads/";
            $target_file = $target_directory . $image;
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);

            $file_upload_error_msg = "";


          // check file is real or not
          $check = getimagesize($_FILES["image"]["tmp_name"]);


          if ($check !== false) {

            // upload file is image
            
          } else {

            $file_upload_error_msg.="<div>

              Upload file is not image.
              
            </div>";
            
          }

          // check file type
          $allowed_file_types = array("jpg", "jpeg", "png");

          if (!in_array($file_type, $allowed_file_types)) {

            $file_upload_error_msg.="<div>

              Only jpg, jpeg and png files are allowed.
              
            </div>";

          }

          // if file does not there
          if (file_exists($target_file)) {

            $file_upload_error_msg.="<div>

              Image is already exists. Please change the file name.
              
            </div>";

          }

          // image size <= 2MB
          if ($_FILES['image']['size'] > (2048000)) {

            $file_upload_error_msg.="<div>

              Image size is larger than 2MB.
              
            </div>";

          }

          // create upload folder
          if (!is_dir($target_directory)) {

            mkdir($target_directory, 0777, true);

          }

          // if file_upload_error_msg is empty
          if (empty($file_upload_error_msg)) {

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

              // image is uploaded
              
            } else {

              echo "<div class='alert alert-danger'>";

                echo "<div>

                  Image is not uploaded.
                  
                </div>";

                echo "<div>

                  Update record to insert image.
                  
                </div>";

              echo "</div>";
              
            }
            
          } else {

            echo "<div class='alert alert-danger'>";

              echo "<div>

                {$file_upload_error_msg}
                
              </div>";

              echo "<div>

                Update record to insert image.
                
              </div>";

            echo "</div>";
            
          }

        }
            
          } else {

            echo "<div class='alert alert-danger'>

              Record is not updated. 
              
            </div>";
            
          }

          
        } catch (PDOException $e) {

          die('ERROR:' . $e->getMessage());
          
        }

       }

        ?>



      <!-- HTML form to enter new data -->
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}"); ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">

        <table class="table table-hover table-bordered">

          <tr>
            <td>Student Name:</td>
            <td><input type="text" name="studentName" value="<?php echo htmlspecialchars($studentName, ENT_QUOTES); ?>" placeholder="Enter Student Name" class="form-control"></td>
          </tr>

          <tr>
            <td>Father Name:</td>
            <td><input type="text" name="fatherName" value="<?php echo htmlspecialchars($fatherName, ENT_QUOTES); ?>" placeholder="Enter Father Name" class="form-control"></td>
          </tr>

          <tr>
            <td>Date of Birth:</td>
            <td><input type="date" name="dob" value="<?php echo htmlspecialchars($dob, ENT_QUOTES); ?>" class="form-control"></td>
          </tr>

          <tr>
            <td>Gender:</td>
            <td>
              <input type="radio" name="gender" value="Male"<?php if ($gender == 'Male') echo 'checked="checked"'; ?>> Male <br>
              <input type="radio" name="gender" value="Female"<?php if ($gender == 'Female') echo 'checked="checked"'; ?>> Female
            </td>
          </tr>

          <tr>
            <td>Mobile No:</td>
            <td><input type="text" name="mobileNo" value="<?php echo htmlspecialchars($mobileNo, ENT_QUOTES); ?>" placeholder="Enter Mobile No" maxlength="11" class="form-control"></td>
          </tr>

          <tr>
            <td>Email:</td>
            <td><input type="email" name="email" value="<?php echo htmlspecialchars($email, ENT_QUOTES); ?>" placeholder="Enter Email" class="form-control"></td>
          </tr>

          <tr>
            <td>Address:</td>
            <td><textarea name="address" rows="3" placeholder="Enter Address" class="form-control"><?php echo htmlspecialchars($address, ENT_QUOTES); ?></textarea></td>
          </tr>

          <tr>
            <td>Study Program:</td>
            <td>
              <select name="studyProgram" class="form-control">
                <option value="BSC (2 Years)"<?php if ($studyProgram == 'BSC (2 Years)') echo 'selected="selected"'; ?>>BSC (2 Years)</option>
                <option value="BS (2 Years)"<?php if ($studyProgram == 'BS (2 Years)') echo 'selected="selected"'; ?>>BS (2 Years)</option>
                <option value="BS (4 Years)"<?php if ($studyProgram == 'BS (4 Years)') echo 'selected="selected"'; ?>>BS (4 Years)</option>
                <option value="MS (2 Years)"<?php if ($studyProgram == 'MS (2 Years)') echo 'selected="selected"'; ?>>MS (2 Years)</option>
                <option value="MSC (2 Years)"<?php if ($studyProgram == 'MSC (2 Years)') echo 'selected="selected"'; ?>>MSC (2 Years)</option>
                <option value="M-Phil"<?php if ($studyProgram == 'M-Phil') echo 'selected="selected"'; ?>>M-Phil</option>
                <option value="PHD"<?php if ($studyProgram == 'PHD') echo 'selected="selected"'; ?>>PHD</option>
              </select>
            </td>
          </tr>

          <tr>
            <td>Hobby:</td>
            <td>

              <?php $hobbies = explode(",", $hobby); ?>

              <input type="checkbox" name="hobby[]" value="Reading"<?php  if (in_array('Reading', $hobbies)){ echo "checked='checked'"; }?>>Reading
              <input type="checkbox" name="hobby[]" value="Sports"<?php  if (in_array('Sports', $hobbies)){ echo "checked='checked'"; }?>>Sports
              <input type="checkbox" name="hobby[]" value="Traveling"<?php  if (in_array('Traveling', $hobbies)){ echo "checked='checked'"; }?>>Traveling
              <input type="checkbox" name="hobby[]" value="None"<?php  if (in_array('None', $hobbies)){ echo "checked='checked'"; }?>>None
            </td>
          </tr>

          <tr>
            <td>Profile Image</td>
            <td>
              <?php echo $image ? "<img class='rounded w-25' src='uploads/{$image}'>" : "No image is found."; ?> <br>
              <input type="file" name="image" class="text-white">
            </td>
          </tr>

          <tr>
            <td></td>
            <td>
              <input type="submit" value="Save Changes" class="btn btn-primary">
              <a href="index.php" class="btn btn-secondary">Back to Records</a>
            </td>
          </tr>
        

        </table>
        
      </form>

      
    </div>

    
    <script src="js/jquery-3.2.1.slim.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/popper.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
  </body>
</html>