<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Records_Page</title>
  </head>
  <body>
    

    <div class="container p-5">

      <h1>Student Records</h1>
      <hr>

      <!-- PHP for read all records -->
      <?php 

      include 'config/database.php';

      // pagination, default page 1
      $page = isset($_GET['page']) ? $_GET['page'] : 1;

      // per page show result
      $records_per_page = 10;

      // query limit
      $from_record_num = ($records_per_page * $page) - $records_per_page;



      // delete message prompt
      $action = isset($_GET['action']) ? $_GET['action'] : "";

      if ($action == 'deleted') {

        echo "<div class='alert alert-success'>

          Record is deleted.
          
        </div>";

      }


      // select all data
      $query = "SELECT id, studentName, fatherName, mobileNo, address FROM records ORDER BY id DESC LIMIT :from_record_num, :records_per_page";
      $statement = $conn->prepare($query);
      $statement->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
      $statement->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
      $statement->execute();

      // get no of rows
      $num = $statement->rowCount();

      // create new link btn
      echo "<a href='create.php' class='btn btn-primary mb-3'>Create New Student Record</a>";

      // check record is there or not
      if ($num > 0) {

        // display database records
        echo "<table class='table table-hover table-bordered'>";

          echo "<tr>";

            echo "<th>ID</th>";
            echo "<th>Student Name</th>";
            echo "<th>Father Name</th>";
            echo "<th>Mobile No</th>";
            echo "<th>Address</th>";
            echo "<th>Action</th>";

          echo "</tr>";

          // retrieve table contents
          while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {

            extract($row);

            // table for per record
            echo "<tr>";

              echo "<td>{$id}</td>";
              echo "<td>{$studentName}</td>";
              echo "<td>{$fatherName}</td>";
              echo "<td>{$mobileNo}</td>";
              echo "<td>{$address}</td>";
              echo "<td>";

                echo "<a href='read_one.php?id={$id}' class='btn btn-info'>Read</a>";
                echo "<a href='update.php?id={$id}' class='btn btn-primary mx-2'>Update</a>";
                echo "<a href='#' onclick='delete_user({$id});' class='btn btn-danger'>Delete</a>";

              echo "</td>";

            echo "</tr>";
              
          }



        echo "</table>";


        // pagination, count total rows
        $query = "SELECT COUNT(*) as total_rows FROM records";
        $statement = $conn->prepare($query);
        $statement->execute();

        // getting total rows
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        $total_rows = $row['total_rows'];

        $page_url = "index.php?";
        include_once "pagination.php";
        

      } else {

        echo "<div class='alert alert-danger'>

          No records found.
          
        </div>";
        
      }


       ?>

      
    </div>


    <!-- delete record confirmaion -->
    <script type="text/javascript" charset="utf-8">

      function delete_user(id) {

        var ans = confirm('Are you sure?');

        if (ans) {

          window.location = 'delete.php?id=' + id;

        }

      }


    </script>

    
    <script src="js/jquery-3.2.1.slim.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/popper.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
  </body>
</html>