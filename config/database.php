<?php 

$host = "localhost";
$db_name = "php_assignment";
$username = "root";
$password = "";

try {

	$conn = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
	
} catch (PDOException $e) {

	echo "Connection error:" . $e->getMessage();
	
}

 ?>