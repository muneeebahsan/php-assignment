
<?php 

include 'config/database.php';

try {

	// get id
	$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');

	// delete query
	$query = "DELETE FROM records WHERE id = ?";
	$statement = $conn->prepare($query);
	$statement->bindParam(1, $id);

	if ($statement->execute()) {

		// go on to index.php
		header('Location: index.php?action=deleted');
		
	} else {

		die('Record is not deleted.');
		
	}
	
} catch (PDOException $e) {

	die('ERROR:' . $e->getMessage());
	
}

 ?>