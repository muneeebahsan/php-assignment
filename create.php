<!doctype html>
<html lang="en">
  <head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Create_Page</title>
  </head>
  <body>
    
    <div class="container p-5">

    	<h1>Create Student Record</h1>
    	<hr>

    	<!-- PHP insert data into database -->
    	<?php 

    	if ($_POST) {

    		include 'config/database.php';

    		try {

    			// query
    			$query = "INSERT INTO records SET studentName=:studentName, fatherName=:fatherName, dob=:dob, gender=:gender, mobileNo=:mobileNo, email=:email, address=:address, studyProgram=:studyProgram, hobby=:hobby, image=:image, created=:created";

    			// prepare for exection
    			$statement = $conn->prepare($query);

    			// posted values
    			$studentName = htmlspecialchars(strip_tags($_POST['studentName']));
    			$fatherName = htmlspecialchars(strip_tags($_POST['fatherName']));
    			$dob = htmlspecialchars(strip_tags($_POST['dob']));
    			$gender = htmlspecialchars(strip_tags($_POST['gender']));
    			$mobileNo = htmlspecialchars(strip_tags($_POST['mobileNo']));
    			$email = htmlspecialchars(strip_tags($_POST['email']));
    			$address = htmlspecialchars(strip_tags($_POST['address']));
    			$studyProgram = htmlspecialchars(strip_tags($_POST['studyProgram']));
    			$hobby = htmlspecialchars(implode(",", $_POST['hobby']));
    			$image =! empty($_FILES["image"]["name"]) ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";
    			$image = htmlspecialchars(strip_tags($image));

    			// bind parameters
    			$statement->bindParam(':studentName', $studentName);
    			$statement->bindParam(':fatherName', $fatherName);
    			$statement->bindParam(':dob', $dob);
    			$statement->bindParam(':gender', $gender);
    			$statement->bindParam(':mobileNo', $mobileNo);
    			$statement->bindParam(':email', $email);
    			$statement->bindParam(':address', $address);
    			$statement->bindParam(':studyProgram', $studyProgram);
    			$statement->bindParam(':hobby', $hobby);
    			$statement->bindParam(':image', $image);

    			// data entry date
    			$created = date('y-m-d h:i:s');
    			$statement->bindParam(':created', $created);

    			// execution
    			if ($statement->execute()) {

    				echo "<div class='alert alert-success'>

    					Record is saved. 
    					
    				</div>";

    			// check image field is nto empty
    			if ($image) {

    				$target_directory = "uploads/";
    				$target_file = $target_directory . $image;
    				$file_type = pathinfo($target_file, PATHINFO_EXTENSION);

    				$file_upload_error_msg = "";


    			// check file is real or not
    			$check = getimagesize($_FILES["image"]["tmp_name"]);


    			if ($check !== false) {

    				// upload file is image
    				
    			} else {

    				$file_upload_error_msg.="<div>

    					Upload file is not image.
    					
    				</div>";
    				
    			}

    			// check file type
    			$allowed_file_types = array("jpg", "jpeg", "png");

    			if (!in_array($file_type, $allowed_file_types)) {

    				$file_upload_error_msg.="<div>

    					Only jpg, jpeg and png files are allowed.
    					
    				</div>";

    			}

    			// if file does not there
    			if (file_exists($target_file)) {

    				$file_upload_error_msg.="<div>

    					Image is already exists. Please change the file name.
    					
    				</div>";

    			}

    			// image size <= 2MB
    			if ($_FILES['image']['size'] > (2048000)) {

    				$file_upload_error_msg.="<div>

    					Image size is larger than 2MB.
    					
    				</div>";

    			}

    			// create upload folder
    			if (!is_dir($target_directory)) {

    				mkdir($target_directory, 0777, true);

    			}

    			// if file_upload_error_msg is empty
    			if (empty($file_upload_error_msg)) {

    				if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

    					// image is uploaded
    					
    				} else {

    					echo "<div class='alert alert-danger'>";

    						echo "<div>

    							Image is not uploaded.
    							
    						</div>";

    						echo "<div>

    							Update record to insert image.
    							
    						</div>";

    					echo "</div>";
    					
    				}
    				
    			} else {

    				echo "<div class='alert alert-danger'>";

    					echo "<div>

    						{$file_upload_error_msg}
    						
    					</div>";

    					echo "<div>

    						Update record to insert image.
    						
    					</div>";

    				echo "</div>";
    				
    			}

            }
    				
    			} else {

    				echo "<div class='alert alert-danger'>

    					Record is not saved.
    					
    				</div>";
    				
    			}
    			

    		} catch (PDOException $e) {

    			die('ERROR:' . $e->getMessage());
    			
    		}
    		
    	}

    	 ?>

    	
    	<!-- HTML Form to create student record -->
    	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">

    		<table class="table table-hover table-bordered">

    			<tr>
    				<td>Student Name:</td>
    				<td><input type="text" name="studentName" placeholder="Enter Student Name" class="form-control"></td>
    			</tr>

    			<tr>
    				<td>Father Name:</td>
    				<td><input type="text" name="fatherName" placeholder="Enter Father Name" class="form-control"></td>
    			</tr>

    			<tr>
    				<td>Date of Birth:</td>
    				<td><input type="date" name="dob" class="form-control"></td>
    			</tr>

    			<tr>
    				<td>Gender:</td>
    				<td>
    					<input type="radio" name="gender" value="Male" checked> Male <br>
    					<input type="radio" name="gender" value="Female"> Female
    				</td>
    			</tr>

    			<tr>
    				<td>Mobile No:</td>
    				<td><input type="text" name="mobileNo" placeholder="Enter Mobile No" maxlength="11" class="form-control"></td>
    			</tr>

    			<tr>
    				<td>Email:</td>
    				<td><input type="email" name="email" placeholder="Enter Email" class="form-control"></td>
    			</tr>

    			<tr>
    				<td>Address:</td>
    				<td><textarea name="address" rows="3" placeholder="Enter Address" class="form-control"></textarea></td>
    			</tr>

    			<tr>
    				<td>Study Program:</td>
    				<td>
    					<select name="studyProgram" class="form-control">
    						<option value="BSC (2 Years)">BSC (2 Years)</option>
    						<option value="BS (2 Years)">BS (2 Years)</option>
    						<option value="BS (4 Years)">BS (4 Years)</option>
    						<option value="MS (2 Years)">MS (2 Years)</option>
    						<option value="MSC (2 Years)">MSC (2 Years)</option>
    						<option value="M-Phil">M-Phil</option>
    						<option value="PHD">PHD</option>
    					</select>
    				</td>
    			</tr>

    			<tr>
    				<td>Hobby:</td>
    				<td>
    					<input type="checkbox" name="hobby[]" value="Reading" checked>Reading
    					<input type="checkbox" name="hobby[]" value="Sports">Sports
    					<input type="checkbox" name="hobby[]" value="Traveling">Traveling
    					<input type="checkbox" name="hobby[]" value="None">None
    				</td>
    			</tr>

    			<tr>
    				<td>Profile Image:</td>
    				<td><input type="file" name="image" class="form-control-file border rounded"></td>
    			</tr>

    			<tr>
    				<td></td>
    				<td>
    					<input type="submit" value="Save" class="btn btn-primary">
    					<input type="reset" value="Reset" class="btn btn-danger">
    					<a href="index.php" class="btn btn-secondary">Back to Records</a>
    				</td>
    			</tr>
    		

    		</table>
    		
    	</form>
    	
    </div>

    
    <script src="js/jquery-3.2.1.slim.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/popper.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
  </body>
</html>