<!doctype html>
<html lang="en">
  <head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>One_Record_Page</title>
  </head>
  <body>
    

    <div class="container p-5">

      <h1>Student Record</h1>
      <hr>

      <!-- PHP one record -->
      <?php 

      // check value is there or not
      $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');

      include 'config/database.php';

      try {

        // prepare query
        $query = "SELECT id, studentName, fatherName, dob, gender, mobileNo, email, address, studyProgram, hobby, image FROM records WHERE id = ? LIMIT 0, 1";
        $statement = $conn->prepare($query);

        $statement->bindParam(1, $id);

        
        $statement->execute();

        // store return value
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        // values goes in form
        $studentName = $row['studentName'];
        $fatherName = $row['fatherName'];
        $dob = $row['dob'];
        $gender = $row['gender'];
        $mobileNo = $row['mobileNo'];
        $email = $row['email'];
        $address = $row['address'];
        $studyProgram = $row['studyProgram'];
        $hobby = $row['hobby'];
        $image = htmlspecialchars($row['image'], ENT_QUOTES);

        
      } catch (PDOException $e) {

        die('ERROR:' . $e->getMessage());
        
      }

       ?>


      <!-- HTML table to display record -->
      <table class="table table-hover table-bordered">


          <tr>
            <td>Student Name:</td>
            <td><?php echo htmlspecialchars($studentName, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Father Name:</td>
            <td><?php echo htmlspecialchars($fatherName, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Date of Birth: (Y-M-D)</td>
            <td><?php echo htmlspecialchars($dob, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Gender:</td>
            <td><?php echo htmlspecialchars($gender, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Mobile No:</td>
            <td><?php echo htmlspecialchars($mobileNo, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Email:</td>
            <td><?php echo htmlspecialchars($email, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Address:</td>
            <td><?php echo htmlspecialchars($address, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Study Program:</td>
            <td><?php echo htmlspecialchars($studyProgram, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Hobby:</td>
            <td><?php echo htmlspecialchars($hobby, ENT_QUOTES); ?></td>
          </tr>

          <tr>
            <td>Profile Image</td>
            <td><?php echo $image ? "<img class='rounded w-25' src='uploads/{$image}'>" : "No image is found."; ?></td>
          </tr>

          <tr>
            <td></td>
            <td><a href="index.php" class="btn btn-secondary">Back to Read Records</a></td>
          </tr>


        </table>

      
    </div>

    
    <script src="js/jquery-3.2.1.slim.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/popper.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
  </body>
</html>